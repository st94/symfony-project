<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Customer;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 50; $i++) {
            $customer = new Customer();
            $customer->setFirstName('Fname' .$i);
            $customer->setLastName('Lname' .$i);
            $customer->setEmail('Fname@gmail.' .$i);
            $customer->setPhoneNumber('+216 000 0' .$i);
            $manager->persist($customer);
        }

        $manager->flush();
    }
}